import React from 'react';
import logo from './logo.svg';
import './App.css';
import { Provider } from "react-redux";
import  store  from './store';
import Post from './component/post';
import 'bootstrap/dist/css/bootstrap.min.css';


function App() {
  return (
    <Provider store={store}>
    
    
    <div className="App">
      <header className="App-header">
          <Post />
      </header>
    </div>
    </Provider>
  );
}

export default App;
