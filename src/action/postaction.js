import {FETCH_POST,NEW_POST} from "../action/type";


export const  fetch_Post = () => dispatch => {
        fetch('https://jsonplaceholder.typicode.com/posts')
        .then(res => res.json())
        .then(posts => dispatch({
            type :FETCH_POST,
            payload : posts 
        }) )

}