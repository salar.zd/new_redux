import React, { Component } from 'react';
import { connect } from 'react-redux';
import {fetch_Post} from '../action/postaction';
import store from "../store";
// import PropTypes from 'props-types';
class post extends Component {
    
    componentDidMount(){
        // this.props.fetch_Post();
        // store.dispatch(login('osama',1234));
        store.dispatch(fetch_Post());
        
    }

    render() {

    const posts =  this.props.posts.map(post => {
            return (<div className="container flex" key={post.id}>
                <div className="row">
                    <div className="col-1">{"Post " +  post.id}</div>
                <div className="col-4">{post.title}</div>
                <div className="col-7">{post.body}</div>
                </div>
                <br/>
            </div>);

        });
        return (
            
            <div>
                 <div className="row">
                    <div className="col-1">ID</div>
                <div className="col-4">Title</div>
                <div className="col-7">Body</div>
                </div>
                {posts}
            </div>
        );
    }
}

// post.propTypes = {
//     FetchPost : PropTypes.func.isRequired,
//     posts : PropTypes.array.isRequired
// }

const mapStateToProps = state => ({

    posts : state.posts.items

});

export default connect(mapStateT